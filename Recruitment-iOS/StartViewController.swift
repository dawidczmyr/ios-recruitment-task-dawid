//
//  StartViewController.swift
//  Recruitment-iOS
//
//  Created by Dawid Ramone on 01/02/2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit
import PureLayout
import RxSwift
import RxCocoa

class StartViewController: UIViewController {
    let disposeBag = DisposeBag()
    let startButton: UIButton = {
        let button = UIButton()
        button.setTitle("START",for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15)
        button.backgroundColor = .clear

        return button
    }()

    override func viewDidLoad() {
        self.view.backgroundColor = .white
        self.view.addSubview(startButton)
        self.startButton.autoCenterInSuperview()

        self.startButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }

                let vc = CustomTabBarController()
                self.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: self.disposeBag)
    }
}
