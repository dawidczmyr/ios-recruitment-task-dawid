//
//  AppDelegate.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let customTabBarController = CustomTabBarController()
    private let startViewController = StartViewController()

    func applicationDidFinishLaunching(_ application: UIApplication) {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: StartViewController())
        window?.makeKeyAndVisible()
    }

}

class CustomTabBarController: UITabBarController {
    private let firstListViewController = FirstListViewController()
    private let secondListViewController = SecondListViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewControllers = [firstListViewController, secondListViewController]
    }

}


