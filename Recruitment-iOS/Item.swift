//
//  ItemModel.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

enum ItemColor: String, Codable {
    case Red
    case Green
    case Blue
    case Yellow
    case Purple

    var color: UIColor {
        switch self {
        case .Red:
            return .red
        case .Green:
            return .green
        case .Blue:
            return .blue
        case .Yellow:
            return .yellow
        case .Purple:
            return .purple
        }
    }
}

struct ItemModels: Codable {
    var data: [Item]?
}

struct ItemModel: Codable {
    var data: Item?
}

struct Attributes: Codable {
    var name: String?
    var preview: String?
    var color: ItemColor?
    var desc: String?
}

struct Item: Codable {
    var id: String?
    var type: String?
    var attributes: Attributes?
}
