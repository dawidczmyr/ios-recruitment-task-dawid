//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NetworkingManager {
    static let sharedManager = NetworkingManager()

    func decode<T: Decodable>(data: Data) throws -> T? {
         do {
             let decoder = JSONDecoder()
             let user = try decoder.decode(T.self, from: data)

             return user
         } catch let error {
             print(error)

             return nil
         }
     }

    func loadItemsFromFiles(filename: String) -> Observable<ItemModels?> {
        guard let fileURL = Bundle.main.url(forResource: filename, withExtension: "json") else {
            print("couldn't find the file")
            return Observable.just(nil)
        }

        do {
            let content = try Data(contentsOf: fileURL)
            let items = try decode(data: content) as ItemModels?
            return Observable.just(items)
        } catch let error {
            print(error)
            return Observable.just(nil)
        }
    }

    func loadItemsDetailsFromFiles(with id: String) -> Observable<ItemModel?> {
        guard let fileURL = Bundle.main.url(forResource: "Item" + id, withExtension: "json") else {
            print("couldn't find the file")

            return Observable.just(nil)
        }

        do {
            let content = try Data(contentsOf: fileURL)
            print(fileURL)
            let item = try decode(data: content) as ItemModel?

            return Observable.just(item)
        } catch let error {
            print(error)

            return Observable.just(nil)
        }
    }
}
