//
//  ItemDetailsViewController.swift
//  Recruitment-iOS
//
//  Created by Dawid Ramone on 02/02/2020.
//  Copyright © 2020 Untitled Kingdom. All rights reserved.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    private let item: ItemModel

    private lazy var tabBarTitle: String? = {
        item.data?.attributes?.name?
            .enumerated()
            .map { $0 % 2 == 0
                ? $1.uppercased()
                : $1.lowercased() }
            .reduce("", +)
    }()

    private lazy var backgroundColor: UIColor = {
        item.data?.attributes?.color?.color ?? .white
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = self.item.data?.attributes?.desc

        return label
    }()

    init(item: ItemModel) {
        self.item = item
        super.init(nibName: nil, bundle: nil)
        self.navigationItem.title = self.tabBarTitle
        self.view.addSubview(self.descriptionLabel)
        self.view.backgroundColor = self.backgroundColor
        self.createConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func createConstraints() {
        self.descriptionLabel.autoPinEdge(toSuperviewMargin: .top)
        self.descriptionLabel.autoPinEdge(toSuperviewEdge: .left)
        self.descriptionLabel.autoPinEdge(toSuperviewEdge: .right)
        self.descriptionLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25, relation: .greaterThanOrEqual)
    }
}
