//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit
import PureLayout
import RxSwift
import RxCocoa
import RxDataSources

class FirstListViewController: UIViewController {
    private let disposeBag: DisposeBag = DisposeBag()
    private let itemsRelay = BehaviorRelay<[Item]>(value: [])

    let tableView: UITableView = {
        let tableView = UITableView()

        return tableView
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }

    override func viewDidLoad() {
        self.title = "TableView"
        self.tabBarController?.title = "List"
        self.view.backgroundColor = .white
        self.addSubviews()
        self.bind()
        self.createConstraints()
    }

    private func addSubviews() {
        self.view.addSubview(self.tableView)
    }

    private func createConstraints() {
        self.tableView.autoPinEdgesToSuperviewEdges()
    }

    func bind() {
        NetworkingManager.sharedManager.loadItemsFromFiles(filename: "Items")
            .delay(.seconds(2), scheduler: MainScheduler.instance)
            .map { ($0?.data ?? []) }
            .bind(to: self.itemsRelay)
            .disposed(by: self.disposeBag)

        itemsRelay.asObservable()
            .bind(to: tableView.rx.items) { [weak self] table, index, element in
                return self?.makeCell(with: element, from: table) ?? UITableViewCell()
            }
            .disposed(by: self.disposeBag)

        self.tableView.rx.itemSelected
            .map { [weak self] intexPath -> String in
                 self?.itemsRelay.value[intexPath.row].id ?? ""
            }
            .flatMap {  NetworkingManager.sharedManager.loadItemsDetailsFromFiles(with: $0) }
            .subscribe(onNext: { [weak self] item in
                guard let item = item else { return }
                let vc = ItemDetailsViewController(item: item)
                self?.navigationController?.pushViewController(vc, animated: true)
            })
            .disposed(by: self.disposeBag)
    }

    private func makeCell(with element: Item, from table: UITableView) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.selectionStyle = .none
        cell.textLabel?.text = element.attributes?.name
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = element.attributes?.preview
        cell.backgroundColor = element.attributes?.color?.color

      return cell
    }

}
